package base

import "testing"

func TestPortRange(t *testing.T) {
	const (
		rangestr_good   = "103,U:200-1024,3000-4000,T:3-4,U:7"
		rangestr_bad    = "103U,T3-5"
		rangestr_sample = "3-5,U:7,8-9,T:10-11"
	)
	if !ValidatePortRange(rangestr_good) {
		t.Errorf(
			"%s failed to validate but should have been valid",
			rangestr_good,
		)
	}
	if ValidatePortRange(rangestr_bad) {
		t.Errorf("%s validated but shouldn't have", rangestr_bad)
	}
	prange, err := PortRangesFromStr(rangestr_sample)
	if err != nil {
		t.Errorf("Error creating ranges from %s: %v", rangestr_sample, err)
	}
	type prtest struct {
		pr    *PortRange
		start int
		end   int
		proto port_protocol
	}
	tests := []prtest{
		prtest{prange[0], 3, 5, PORT_PROTOCOL_TCP},
		prtest{prange[1], 7, 0, PORT_PROTOCOL_UDP},
		prtest{prange[2], 8, 9, PORT_PROTOCOL_UDP},
	}
	for _, prt := range tests {
		pr := prt.pr
		if pr.Start != prt.start {
			t.Errorf("Incorrect start on test range. Got %d, expected %d", pr.Start, prt.start)
		}
		if pr.End != prt.end {
			t.Errorf("Incorrect end on test range. Got %d, expected %d", pr.End, prt.end)
		}
		if pr.Protocol != prt.proto {
			t.Errorf("Incorrect protocol. Got %v, expected %v", pr.Protocol, prt.proto)
		}
	}

}
