package base

import "testing"

func TestPref(t *testing.T) {
	np := NewNVTPref("foo", "bar", "baz")
	if np.Name() != "foo" {
		t.Errorf("Incorrect name returned: %s", np.Name())
	}
	if np.Type() != "bar" {
		t.Errorf("Incorrect type returned: %s", np.Type())
	}
	if np.Default() != "baz" {
		t.Errorf("Incorrect default returned: %s", np.Default())
	}
	np.Free()
	if np.Name() != "" || np.Type() != "" || np.Default() != "" {
		t.Errorf("NvtPref didn't clear properly: %v", np)
	}
}

func TestNvtiGetSet(t *testing.T) {
	n := NewNvti()
	n.SetOid("Oid")
	if n.Oid() != "Oid" {
		t.Errorf(
			"Oid didn't set correctly; got %v but expected %v",
			n.Oid(),
			"Oid",
		)
	}

	n.SetVersion("Version")
	if n.Version() != "Version" {
		t.Errorf(
			"Version didn't set correctly; got %v but expected %v",
			n.Version(),
			"Version",
		)
	}

	n.SetName("Name")
	if n.Name() != "Name" {
		t.Errorf(
			"Name didn't set correctly; got %v but expected %v",
			n.Name(),
			"Name",
		)
	}

	n.SetSummary("Summary")
	if n.Summary() != "Summary" {
		t.Errorf(
			"Summary didn't set correctly; got %v but expected %v",
			n.Summary(),
			"Summary",
		)
	}

	n.SetCopyright("Copyright")
	if n.Copyright() != "Copyright" {
		t.Errorf(
			"Copyright didn't set correctly; got %v but expected %v",
			n.Copyright(),
			"Copyright",
		)
	}

	n.SetCves([]string{"Cves"})
	if n.Cves()[0] != "Cves" {
		t.Errorf(
			"Cves didn't set correctly; got %v but expected %v",
			n.Cves(),
			[]string{"Cves"},
		)
	}

	n.SetBids([]string{"Bids"})
	if n.Bids()[0] != "Bids" {
		t.Errorf(
			"Bids didn't set correctly; got %v but expected %v",
			n.Bids(),
			[]string{"Bids"},
		)
	}

	n.SetXrefs([]string{"Xrefs"})
	if n.Xrefs()[0] != "Xrefs" {
		t.Errorf(
			"Xrefs didn't set correctly; got %v but expected %v",
			n.Xrefs(),
			[]string{"Xrefs"},
		)
	}

	n.SetTags([]string{"Tags"})
	if n.Tags()[0] != "Tags" {
		t.Errorf(
			"Tags didn't set correctly; got %v but expected %v",
			n.Tags(),
			[]string{"Tags"},
		)
	}

	n.SetCvssBase("CvssBase")
	if n.CvssBase() != "CvssBase" {
		t.Errorf(
			"CvssBase didn't set correctly; got %v but expected %v",
			n.CvssBase(),
			"CvssBase",
		)
	}

	n.SetDependencies([]string{"Dependencies"})
	if n.Dependencies()[0] != "Dependencies" {
		t.Errorf(
			"Dependencies didn't set correctly; got %v but expected %v",
			n.Dependencies(),
			[]string{"Dependencies"},
		)
	}

	n.SetRequiredKeys([]string{"RequiredKeys"})
	if n.RequiredKeys()[0] != "RequiredKeys" {
		t.Errorf(
			"RequiredKeys didn't set correctly; got %v but expected %v",
			n.RequiredKeys(),
			[]string{"RequiredKeys"},
		)
	}

	n.SetMandatoryKeys([]string{"MandatoryKeys"})
	if n.MandatoryKeys()[0] != "MandatoryKeys" {
		t.Errorf(
			"MandatoryKeys didn't set correctly; got %v but expected %v",
			n.MandatoryKeys(),
			[]string{"MandatoryKeys"},
		)
	}

	n.SetExcludedKeys([]string{"ExcludedKeys"})
	if n.ExcludedKeys()[0] != "ExcludedKeys" {
		t.Errorf(
			"ExcludedKeys didn't set correctly; got %v but expected %v",
			n.ExcludedKeys(),
			[]string{"ExcludedKeys"},
		)
	}

	n.SetRequiredPorts([]int64{1})
	if n.RequiredPorts()[0] != 1 {
		t.Errorf(
			"RequiredPorts didn't set correctly; got %v but expected %v",
			n.RequiredPorts(),
			[]int64{1},
		)
	}

	n.SetRequiredUdpPorts([]int64{1})
	if n.RequiredUdpPorts()[0] != 1 {
		t.Errorf(
			"RequiredUdpPorts didn't set correctly; got %v but expected %v",
			n.RequiredUdpPorts(),
			[]int64{1},
		)
	}

	pref := NewNVTPref("foo", "bar", "baz")
	n.SetPrefs([]*NvtPref{pref})
	if n.Prefs()[0] != pref {
		t.Errorf(
			"Prefs didn't set correctly; got %v but expected %v",
			[]*NvtPref{pref}, n.Prefs(),
		)
	}

	n.SetTimeout(1)
	if n.Timeout() != 1 {
		t.Errorf(
			"Timeout didn't set correctly; got %v but expected %v",
			n.Timeout(),
			1,
		)
	}

	n.SetCategory(ACT_INIT)
	if n.Category() != ACT_INIT {
		t.Errorf(
			"Category didn't set correctly; got %v but expected %v",
			n.Category(),
			ACT_INIT,
		)
	}

	n.SetFamily("Family")
	if n.Family() != "Family" {
		t.Errorf(
			"Family didn't set correctly; got %v but expected %v",
			n.Family(),
			"Family",
		)
	}
}

func TestNvtiFile(t *testing.T) {
	t.Error("No NVTI file tests yet - need to find a sample file")
}
