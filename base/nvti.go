package base

/* Implementation of an API to handle Network Vulnerability Test (NVT) Info datasets */

import (
	"strings"

	gkeyfile "bitbucket.org/pkohler/gkeyfile"
)

type NvtCategory int64

// Categories actually come from misc/nvt_categories.c, but make more sense to declare here
const (
	ACT_INIT NvtCategory = iota
	ACT_SCANNER
	ACT_SETTINGS
	ACT_GATHER_INFO
	ACT_ATTACK
	ACT_MIXED_ATTACK
	ACT_DESTRUCTIVE_ATTACK
	ACT_DENIAL
	ACT_KILL_HOST
	ACT_FLOOD
	ACT_END
	ACT_UNKNOWN
	ACT_FIRST = ACT_INIT
	ACT_LAST  = ACT_END

	ACT_STRING_INIT               = "init"
	ACT_STRING_SCANNER            = "scanner"
	ACT_STRING_SETTINGS           = "settings"
	ACT_STRING_GATHER_INFO        = "infos"
	ACT_STRING_ATTACK             = "attack"
	ACT_STRING_MIXED_ATTACK       = "mixed"
	ACT_STRING_DESTRUCTIVE_ATTACK = "destructive_attack"
	ACT_STRING_DENIAL             = "denial"
	ACT_STRING_KILL_HOST          = "kill_host"
	ACT_STRING_FLOOD              = "flood"
	ACT_STRING_END                = "end"
	ACT_STRING_UNKNOWN            = "unknown"
)

type Nvti struct {
	/*
	 * The structure of a information record that corresponds to a NVT.
	 *
	 * The elements of this structure should never be accessed directly.
	 * Only the functions corresponding to this module should be used.
	 */
	oid              string     //Object ID
	version          string     // Version of the NVT
	name             string     // The name
	summary          string     //Summary about the NVT
	copyright        string     // Copyright for the NVT
	cves             []string   // CVEs this NVT corresponds to
	bids             []string   // Bugtraq IDs this NVT corresponds to
	xrefs            []string   // Cross-references this NVT corresponds to
	tags             []string   //List of tagss attached to this NVT
	cvssBase         string     // CVSS base score for this nvt
	dependencies     []string   // List of dependencies of this NVT
	requiredKeys     []string   // required KB keys of this NVT
	mandatoryKeys    []string   // List of mandatory KB keys of this NVT
	excludedKeys     []string   // List of excluded KB keys of this NVT
	requiredPorts    []int64    // List of required ports of this NVT
	requiredUdpPorts []int64    // list of required UDP ports of this NVT
	prefs            []*NvtPref // collection of NVT preferences
	// the following are not settled yet
	timeout  int64       // Default timeout time for this NVT
	category NvtCategory //The category this NVT belongs to
	family   string      //Family the NVT belongs to
}

func NewNvti() *Nvti {
	return &Nvti{}
}

func (n *Nvti) Free() {
	n = NewNvti()
}

func (n *Nvti) Oid() string               { return n.oid }
func (n *Nvti) Version() string           { return n.version }
func (n *Nvti) Name() string              { return n.name }
func (n *Nvti) Summary() string           { return n.summary }
func (n *Nvti) Copyright() string         { return n.copyright }
func (n *Nvti) Cves() []string            { return n.cves }
func (n *Nvti) Bids() []string            { return n.bids }
func (n *Nvti) Xrefs() []string           { return n.xrefs }
func (n *Nvti) Tags() []string            { return n.tags }
func (n *Nvti) CvssBase() string          { return n.cvssBase }
func (n *Nvti) Dependencies() []string    { return n.dependencies }
func (n *Nvti) RequiredKeys() []string    { return n.requiredKeys }
func (n *Nvti) MandatoryKeys() []string   { return n.mandatoryKeys }
func (n *Nvti) ExcludedKeys() []string    { return n.excludedKeys }
func (n *Nvti) RequiredPorts() []int64    { return n.requiredPorts }
func (n *Nvti) RequiredUdpPorts() []int64 { return n.requiredUdpPorts }
func (n *Nvti) Prefs() []*NvtPref         { return n.prefs }
func (n *Nvti) Timeout() int64            { return n.timeout }
func (n *Nvti) Category() NvtCategory     { return n.category }
func (n *Nvti) Family() string            { return n.family }

func (n *Nvti) SetOid(Oid string) {
	n.oid = Oid
}
func (n *Nvti) SetVersion(Version string) {
	n.version = Version
}
func (n *Nvti) SetName(Name string) {
	n.name = Name
}
func (n *Nvti) SetSummary(Summary string) {
	n.summary = Summary
}
func (n *Nvti) SetCopyright(Copyright string) {
	n.copyright = Copyright
}
func (n *Nvti) SetCves(Cves []string) {
	n.cves = Cves
}
func (n *Nvti) SetBids(Bids []string) {
	n.bids = Bids
}
func (n *Nvti) SetXrefs(Xrefs []string) {
	n.xrefs = Xrefs
}
func (n *Nvti) SetTags(Tags []string) {
	n.tags = Tags
}
func (n *Nvti) SetCvssBase(CvssBase string) {
	n.cvssBase = CvssBase
}
func (n *Nvti) SetDependencies(Dependencies []string) {
	n.dependencies = Dependencies
}
func (n *Nvti) SetRequiredKeys(RequiredKeys []string) {
	n.requiredKeys = RequiredKeys
}
func (n *Nvti) SetMandatoryKeys(MandatoryKeys []string) {
	n.mandatoryKeys = MandatoryKeys
}
func (n *Nvti) SetExcludedKeys(ExcludedKeys []string) {
	n.excludedKeys = ExcludedKeys
}
func (n *Nvti) SetRequiredPorts(RequiredPorts []int64) {
	n.requiredPorts = RequiredPorts
}
func (n *Nvti) SetRequiredUdpPorts(RequiredUdpPorts []int64) {
	n.requiredUdpPorts = RequiredUdpPorts
}
func (n *Nvti) SetPrefs(Prefs []*NvtPref) {
	n.prefs = Prefs
}
func (n *Nvti) SetTimeout(Timeout int64) {
	n.timeout = Timeout
}
func (n *Nvti) SetCategory(Category NvtCategory) {
	n.category = Category
}
func (n *Nvti) SetFamily(Family string) {
	n.family = Family
}

func NvtiFromKeyfile(filename string) *Nvti {
	keyfile := gkeyfile.NewGKeyFile()
	keyfile.LoadFromFile(filename, gkeyfile.G_KEY_FILE_NONE)
	niMap := keyfile.LookupGroup("NVT Info").LookupMap
	OID, _ := niMap["OID"].GetStringValue()
	Version, _ := niMap["Version"].GetStringValue()
	Name, _ := niMap["Name"].GetStringValue()
	Summary, _ := niMap["Summary"].GetStringValue()
	Copyright, _ := niMap["Copyright"].GetStringValue()
	CVEs, _ := niMap["CVEs"].GetStringList()
	BIDs, _ := niMap["BIDs"].GetStringList()
	XREFs, _ := niMap["XREFs"].GetStringList()
	Tags, _ := niMap["Tagss"].GetStringList()
	Dependencies, _ := niMap["Dependencies"].GetStringList()
	RequiredKeys, _ := niMap["RequiredKeys"].GetStringList()
	MandatoryKeys, _ := niMap["MandatoryKeys"].GetStringList()
	ExcludedKeys, _ := niMap["ExcludedKeys"].GetStringList()
	RequiredPorts, _ := niMap["RequiredPorts"].GetIntList()
	RequiredUDPPorts, _ := niMap["RequiredUDPPorts"].GetIntList()
	Family, _ := niMap["Family"].GetStringValue()
	Timeout, _ := niMap["Timeout"].GetIntValue()
	CatInt, _ := niMap["Category"].GetIntValue()
	Category := NvtCategory(CatInt)
	nvti := &Nvti{
		oid:              OID,
		version:          Version,
		name:             Name,
		summary:          Summary,
		copyright:        Copyright,
		cves:             CVEs,
		bids:             BIDs,
		xrefs:            XREFs,
		tags:             Tags,
		dependencies:     Dependencies,
		mandatoryKeys:    MandatoryKeys,
		excludedKeys:     ExcludedKeys,
		requiredPorts:    RequiredPorts,
		requiredKeys:     RequiredKeys,
		requiredUdpPorts: RequiredUDPPorts,
		timeout:          Timeout,
		category:         Category,
		family:           Family,
	}
	if keyfile.LookupGroup("NVT Prefs") != nil {
		g := keyfile.LookupGroup("NVT Prefs")
		prefs := []*NvtPref{}
		for _, pair := range g.KeyValuePairs {
			vals, err := pair.GetStringList()
			if err != nil || len(vals) != 3 {
				// not a valid pref entry
				// TODO: handle this somehow
			}
			prefs = append(
				prefs,
				&NvtPref{
					name:  vals[0],
					ptype: vals[1],
					deflt: vals[2],
				},
			)
		}
		nvti.SetPrefs(prefs)
	}
	return nvti
}

func SetKeyfileInfo(keyfile *gkeyfile.GKeyFile, key string, value string) {
	keyfile.SetString("NVT Info", key, value)
}

func (n *Nvti) ToKeyfile(filename string) error {
	g := gkeyfile.NewGKeyFile()
	SetKeyfileInfo(g, "OID", n.oid)
	SetKeyfileInfo(g, "Version", n.version)
	SetKeyfileInfo(g, "Name", n.name)
	SetKeyfileInfo(g, "Summary", n.summary)
	SetKeyfileInfo(g, "Copyright", n.copyright)
	SetKeyfileInfo(g, "CVEs", strings.Join(n.cves, ";"))
	SetKeyfileInfo(g, "BIDs", strings.Join(n.bids, ";"))
	SetKeyfileInfo(g, "XREFs", strings.Join(n.xrefs, ";"))
	SetKeyfileInfo(g, "Tagss", strings.Join(n.tags, ";"))
	SetKeyfileInfo(g, "Dependencies", strings.Join(n.dependencies, ";"))
	SetKeyfileInfo(g, "RequiredKeys", strings.Join(n.requiredKeys, ";"))
	SetKeyfileInfo(g, "MandatoryKeys", strings.Join(n.mandatoryKeys, ";"))
	SetKeyfileInfo(g, "ExcludedKeys", strings.Join(n.excludedKeys, ";"))
	SetKeyfileInfo(g, "Family", n.family)
	g.SetValue("NVT Info", "RequiredPorts", n.requiredPorts, gkeyfile.G_KEY_FILE_TYPE_INT_LIST)
	g.SetValue("NVT Info", "RequiredUDPPorts", n.requiredUdpPorts, gkeyfile.G_KEY_FILE_TYPE_INT_LIST)
	g.SetValue("NVT Info", "Timeout", n.timeout, gkeyfile.G_KEY_FILE_TYPE_INT)
	g.SetValue("NVT Info", "Category", n.category, gkeyfile.G_KEY_FILE_TYPE_INT)
	for _, p := range n.Prefs() {
		g.SetValue(
			"NVT Prefs",
			p.Name(),
			[]string{p.Name(), p.Type(), p.Default()},
			gkeyfile.G_KEY_FILE_TYPE_STR_LIST,
		)
	}
	return g.SaveToFile(filename)
}

func (nc NvtCategory) IsSafe() bool {
	if nc == ACT_DESTRUCTIVE_ATTACK ||
		nc == ACT_KILL_HOST ||
		nc == ACT_FLOOD ||
		nc == ACT_DENIAL {
		return false
	} else {
		return true
	}
}
