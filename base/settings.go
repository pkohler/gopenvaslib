package base

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	gkeyfile "bitbucket.org/pkohler/gkeyfile"
)

type Settings struct {
	filename  string
	groupname string
	keyfile   *gkeyfile.GKeyFile
}

type SettingsIterator struct {
	keys        []string
	settings    *Settings
	current_key int
	last_key    int
}

func (s *Settings) InitFromFile(filename string, group string) error {
	if strings.TrimSpace(filename) == "" || strings.TrimSpace(group) == "" {
		return fmt.Errorf("Invalid filename (%s) or group (%s) for Settings init.")
	}
	f, err := os.Open(filename)
	if err != nil {
		return fmt.Errorf("Unable to open file %s", filename)
	}
	r := bufio.NewScanner(f)
	gkf := "[Misc]\n"
	for r.Scan() {
		gkf += r.Text() + "\n"
	}
	g := gkeyfile.NewGKeyFile()
	err = g.LoadFromData(
		gkf,
		gkeyfile.G_KEY_FILE_KEEP_COMMENTS|gkeyfile.G_KEY_FILE_KEEP_TRANSLATIONS,
	)
	if err != nil {
		return err
	}
	s.groupname = group
	s.filename = filename
	s.keyfile = g
	return nil
}

func (s *Settings) Cleanup() {
	s.filename = ""
	s.groupname = ""
	s.keyfile = nil
}

func (si *SettingsIterator) InitFromFile(filename string, group string) error {
	s := &Settings{}
	err := s.InitFromFile(filename, group)
	if err != nil {
		return err
	}
	si.settings = s
	si.keys, err = s.keyfile.GetKeys(group)
	if err != nil {
		return err
	}
	si.current_key = 0
	si.last_key = len(si.keys) - 1
	return nil
}

func (si *SettingsIterator) Cleanup() {
	si.keys = []string{}
	si.settings.Cleanup()
	si.settings = nil
	si.current_key = -1
	si.last_key = -1
}

func (si *SettingsIterator) Name() string {
	return si.keys[si.current_key]
}

func (si *SettingsIterator) Next() bool {
	if si.current_key == si.last_key {
		return false
	} else {
		si.current_key++
		return true
	}
}

func (si *SettingsIterator) Value() (
	interface{},
	gkeyfile.GKeyFileValueType,
	error,
) {
	return si.settings.keyfile.GetValue(
		si.settings.groupname,
		si.Name(),
	)
}

func (si *SettingsIterator) NextValue() (
	interface{},
	gkeyfile.GKeyFileValueType,
	error,
) {
	if !si.Next() {
		return nil, -1, fmt.Errorf("EOF")
	} else {
		return si.Value()
	}
}
