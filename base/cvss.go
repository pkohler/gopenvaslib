package base

import (
	"fmt"
	"strings"
)

/**
 * Namely a calculator for the CVSS base score from a CVSS base vector
 * Calculations and values are per the Forum of Incident Response and Security
 * Teams's CVSS guide: https://www.first.org/cvss/v2/guide
 */

const (
	/* AccessVector Constants */
	AV_NETWORK          = 1.0
	AV_ADJACENT_NETWORK = 0.646
	AV_LOCAL            = 0.395

	/* AccessComplexity Constants */
	AC_LOW    = 0.71
	AC_MEDIUM = 0.61
	AC_HIGH   = 0.35

	/* Authentication Constants */
	Au_MULTIPLE_INSTANCES = 0.45
	Au_SINGLE_INSTANCE    = 0.56
	Au_NONE               = 0.704

	/* ConfidentialityImpact Constants */
	C_NONE     = 0.0
	C_PARTIAL  = 0.275
	C_COMPLETE = 0.660

	/* IntegrityImpact Constants */
	I_NONE     = 0.0
	I_PARTIAL  = 0.275
	I_COMPLETE = 0.660

	/* AvailabilityImpact Constants */
	A_NONE     = 0.0
	A_PARTIAL  = 0.275
	A_COMPLETE = 0.660
)

type base_metric int

const (
	Availability base_metric = iota
	Integrity
	Confidentiality
	Authentication
	AccessComplexity
	AccessVector
)

var metricStringMap = map[string]base_metric{
	"A":                Availability,
	"I":                Integrity,
	"C":                Confidentiality,
	"Au":               Authentication,
	"AC":               AccessComplexity,
	"AV":               AccessVector,
	"Availability":     Availability,
	"Integrity":        Integrity,
	"Confidentiality":  Confidentiality,
	"Authentication":   Authentication,
	"AccessComplexity": AccessComplexity,
	"AccessVector":     AccessVector,
}

var metricMap = map[base_metric]string{
	Availability:     "Availability",
	Integrity:        "Integrity",
	Confidentiality:  "Confidentiality",
	Authentication:   "Authentication",
	AccessComplexity: "AccessComplexity",
	AccessVector:     "AccessVector",
}

type impactItem struct {
	Name   string
	NValue float32
}

var impactMap = map[base_metric][3]impactItem{
	Availability: {
		{"N", A_NONE},
		{"P", A_PARTIAL},
		{"C", A_COMPLETE},
	},
	Integrity: {
		{"N", I_NONE},
		{"P", I_PARTIAL},
		{"C", I_COMPLETE},
	},
	Confidentiality: {
		{"N", C_NONE},
		{"P", C_PARTIAL},
		{"C", C_COMPLETE},
	},
	Authentication: {
		{"N", Au_NONE},
		{"M", Au_MULTIPLE_INSTANCES},
		{"S", Au_SINGLE_INSTANCE},
	},
	AccessVector: {
		{"N", AV_NETWORK},
		{"A", AV_ADJACENT_NETWORK},
		{"L", AV_LOCAL},
	},
	AccessComplexity: {
		{"L", AC_LOW},
		{"M", AC_MEDIUM},
		{"H", AC_HIGH},
	},
}

func MetricToEnum(metric string) (base_metric, error) {
	// 0 is only a valid return for Availability
	if metricStringMap[metric] != 0 || (metric == "A" || metric == "Availability") {
		return metricStringMap[metric], nil
	}
	return -1, fmt.Errorf("Unknown metric shorthand %s", metric)
}

func MetricFromEnum(metric base_metric) (string, error) {
	if metricMap[metric] != "" {
		return metricMap[metric], nil
	}
	return "", fmt.Errorf("Invalid base_metric %d", metric)
}

type CVSS struct {
	ConfidentialityImpact float32
	IntegrityImpact       float32
	AvailabilityImpact    float32
	AccessVector          float32
	AcesssComplexity      float32
	Authentication        float32
}

func (c *CVSS) GetImpactSubscore() float32 {
	// Impact = 10.41 * ( 1- (1-ConfImpact)*(1-IntegImpact)*(1-AvailImpact) )
	var score float32
	score = 10.41
	subscore := (1 - c.ConfidentialityImpact)
	subscore *= (1 - c.IntegrityImpact)
	subscore *= (1 - c.AvailabilityImpact)
	score *= (1 - subscore)
	return score
}

func (c *CVSS) GetExploitabilitySubscore() float32 {
	// Exploitability = 20 * AccessVector*AccessComplexity*Authentication
	return 20 * c.AccessVector * c.AcesssComplexity * c.Authentication
}

func (c *CVSS) SetImpactFromStr(val string, metric base_metric) error {
	for i := 0; i < 3; i++ {
		// flip through the three options for each Impact type
		// to find the right one, then set the appropriate value
		// to that variable in the CVSS struct
		impact := impactMap[metric][i]
		if impact.Name == val {
			switch metric {
			case Availability:
				c.AvailabilityImpact = impact.NValue
				return nil
			case Integrity:
				c.IntegrityImpact = impact.NValue
				return nil
			case Confidentiality:
				c.ConfidentialityImpact = impact.NValue
				return nil
			case Authentication:
				c.Authentication = impact.NValue
				return nil
			case AccessVector:
				c.AccessVector = impact.NValue
				return nil
			case AccessComplexity:
				c.AcesssComplexity = impact.NValue
				return nil
			}
		}
	}
	m, err := MetricFromEnum(metric)
	if err != nil {
		return err
	}
	return fmt.Errorf(
		"ImpactMap doesn't contain a value %s for %s",
		m,
		val,
	)
}

func (c *CVSS) GetCVSSScore() float32 {
	/*
	 * Base score is ((0.6*Impact) + (0.4*Exploitability) – 1.5) * f(Impact)
	 * f(impact) is 0 if Impact is 0; otherwise it's 1.176
	 */
	var fimpact, impact, exploitability, score float32
	impact = c.GetImpactSubscore()
	if impact < 0.1 {
		// whole function will become 0 if the final multiplier is 0
		return 0.0
	} else {
		fimpact = 1.176
	}
	impact *= 0.6
	exploitability = c.GetExploitabilitySubscore() * 0.4
	score = (impact + exploitability - 1.5) * fimpact
	return score
}

func GetCVSSScoreFromBaseMetrics(cvss_str string) (float32, error) {
	c, err := GetCVSSFromBaseMetrics(cvss_str)
	if err != nil {
		return 0.0, err
	}
	return c.GetCVSSScore(), nil
}

func GetCVSSFromBaseMetrics(cvss_str string) (*CVSS, error) {
	/* CVSS vectors containing only base metrics take the following form:
	 * (AV:[L,A,N]/AC:[H,M,L]/Au:[N,S,M]/C:[N,P,C]/I:[N,P,C]/A:[N,P,C])
	 * https://nvd.nist.gov/CVSS/Vector-v2.aspx
	 */
	c := &CVSS{}
	// remove brackets if they're there
	cvss_str = strings.Replace(cvss_str, "(", "", -1)
	cvss_str = strings.Replace(cvss_str, ")", "", -1)
	// split into individual metric scores
	cvssplit := strings.Split(cvss_str, "/")
	for _, val := range cvssplit {
		var metric_name, metric_value string
		var mval base_metric
		// split into score's name/value pair
		nvsplit := strings.Split(val, ":")
		if len(nvsplit) != 2 {
			return nil, fmt.Errorf(
				"Base metric %s did not split correctly by ':'",
				val,
			)
		}
		metric_name = nvsplit[0]
		metric_value = nvsplit[1]
		mval, err := MetricToEnum(metric_name)
		if err != nil {
			return nil, err
		}
		c.SetImpactFromStr(metric_value, mval)
	}
	return c, nil
}
