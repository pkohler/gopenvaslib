package base

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

type port_protocol int

const (
	PORT_PROTOCOL_TCP port_protocol = iota
	PORT_PROTOCOL_UDP
	PORT_PROTOCOL_OTHER
)

type PortRange struct {
	Comment  string
	Id       string // uid
	Start    int    // start port
	End      int    // 0 for single port
	Exclude  bool
	Protocol port_protocol
}

func (p *PortRange) Clear() {
	p.Comment = ""
	p.Id = ""
	p.Start = 0
	p.End = 0
	p.Exclude = false
	p.Protocol = PORT_PROTOCOL_TCP
}

func ValidatePortRange(rangestr string) bool {
	/*
	 * Validates a port range string.
	 * Accepts strings in the form of "103,U:200-1024,3000-4000,T:3-4,U:7"
	 * Ranges can be comma and/or newline-separated.
	 */
	split := splitRangeStr(rangestr)
	for _, s := range split {
		s = strings.TrimSpace(s)
		// make sure the range string has the right form
		if !isValidPortRangeFormat(s) {
			return false
		}
		_, err := newPortRange(s)
		if err != nil {
			fmt.Println(err)
			return false
		}
	}
	return true
}

func splitRangeStr(rangestr string) []string {
	splitn := strings.Split(rangestr, "\n")
	split := []string{}
	for _, s := range splitn {
		split = append(split, strings.Split(s, ",")...)
	}
	return split
}

func newPortRange(rangestr string) (*PortRange, error) {
	// create a single PortRange from a string, which should already have been
	// trimmed for whitespace and validated that it's in the correct form
	r := &PortRange{}
	r.Clear()
	r.Protocol = portProtocolIndicator(rangestr)
	rangestr = strings.Replace(rangestr, "T:", "", -1)
	rangestr = strings.Replace(rangestr, "U:", "", -1)
	if strings.Contains(rangestr, "-") {
		split := strings.Split(rangestr, "-")
		first, err1 := strconv.Atoi(strings.TrimSpace(split[0]))
		last, err2 := strconv.Atoi(strings.TrimSpace(split[1]))
		if err1 != nil || err2 != nil {
			return nil, fmt.Errorf(
				"Failed to parse %s: %v %v",
				rangestr,
				err1,
				err2,
			)
		}
		if first > last {
			return nil, fmt.Errorf("First (%d) > Last (%d)", first, last)
		}
		if !isValidPortNumber(first) {
			return nil, fmt.Errorf("Start was %d, must be 0-65535", first)
		}
		if !isValidPortNumber(last) {
			return nil, fmt.Errorf("End was %d, must be 0-65535", last)
		}
		r.Start = first
		r.End = last
	} else {
		num, err := strconv.Atoi(rangestr)
		if err != nil {
			return nil, err
		}
		if !isValidPortNumber(num) {
			return nil, fmt.Errorf("Port was %d, must be 0-65535", num)
		}
		r.Start = num
	}
	return r, nil
}

func isValidPortRangeFormat(rangestr string) bool {
	validRange := regexp.MustCompile(`^([UT]:)?\s*(\d+)(\s*-\s*(\d+))?$`)
	return validRange.MatchString(rangestr)
}

func isValidPortNumber(port int) bool {
	return (port > 0 && port < 65535)
}

func portProtocolIndicator(rangestr string) port_protocol {
	firstchr := strings.Split(rangestr, "")[0]
	if firstchr == "U" {
		return PORT_PROTOCOL_UDP
	} else if firstchr == "T" {
		return PORT_PROTOCOL_TCP

	} else {
		return PORT_PROTOCOL_OTHER
	}
}

func PortRangesFromStr(rangestr string) ([]*PortRange, error) {
	// create a range array from a portrange string
	pranges := []*PortRange{}
	tcp := true
	for _, s := range splitRangeStr(rangestr) {
		/*
		 * Accepts T: and U: before any of the ranges.  This toggles the
		 * remaining ranges, as in nmap.  Treats a leading naked range as TCP,
		 * whereas nmap treats it as TCP and UDP.
		 */
		s = strings.TrimSpace(s)
		switch portProtocolIndicator(s) {
		case PORT_PROTOCOL_TCP:
			tcp = true
		case PORT_PROTOCOL_UDP:
			tcp = false
		case PORT_PROTOCOL_OTHER:
			{
				// leave tcp as whatever it was previously and prepend the
				// indicator to the string so the PortRange picks it up properly
				if tcp {
					s = "T:" + s
				} else {
					s = "U:" + s
				}
			}
		}
		if !isValidPortRangeFormat(s) {
			return nil, fmt.Errorf("Invalid range formatting: %s", s)
		}
		p, err := newPortRange(s)
		if err != nil {
			return nil, err
		}
		pranges = append(pranges, p)
	}
	return pranges, nil
}
