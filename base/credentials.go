package base

type Credentials struct {
	Username        string
	Password        string
	UUID            string
	Timezone        string
	DefaultSeverity float32
	SeverityClass   string
	DynamicSeverity int
	Role            string
}

func (c *Credentials) Free() {
	c.Username = ""
	c.Password = ""
	c.Timezone = ""
	c.Role = ""
	c.SeverityClass = ""
	c.DynamicSeverity = 0
}

func (c *Credentials) AppendToUsername(usr string) {
	c.Username += usr
}

func (c *Credentials) AppendToPassword(pwd string) {
	c.Password += pwd
}
