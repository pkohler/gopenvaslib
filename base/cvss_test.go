package base

import "testing"

func TestMetrics(t *testing.T) {
	base, err := MetricToEnum("A")
	if base != 0 || err != nil {
		t.Errorf(
			"Malformed Availability metric from 'A' - %v %v",
			base,
			err,
		)
	}
	base, err = MetricToEnum("B")
	if err == nil {
		t.Error("'B' should not be a valid metric")
	}
	str, err := MetricFromEnum(Availability)
	if str != "Availability" || err != nil {
		t.Errorf(
			"Malformed metric string from `Availability` - %v %v",
			str,
			err,
		)
	}
	str, err = MetricFromEnum(base_metric(-1))
	if str != "" || err == nil {
		t.Error("Should not get a valid string from an invalid base_metric")
	}
}

func TestCVSS(t *testing.T) {
	cvssstr := "(AV:L/AC:H/Au:N/C:N/I:N/A:C)"
	score, err := GetCVSSScoreFromBaseMetrics(cvssstr)
	if score != 3.9995575 {
		t.Errorf("Score was miscalculated; received %v", score)
	}
	if err != nil {
		t.Error(err.Error())
	}
}
