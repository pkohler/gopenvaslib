package base

import (
	"os"
	"path/filepath"
)

/*
 * Implementation of API to handle NVT Info Cache
 *
 * This file contains all methods to handle NVT Information Cache
 * (nvticache_t).
 */

type NvtiCache struct {
	cachePath string // directory of the cache files
	srcPath   string // directory of the source files
	cacheKb   string // TODO: kb_t is probably not a string
}

func (nc *NvtiCache) Initialized() bool {
	return nc.cacheKb != "" // TODO: kb_t is probably not a string
}

func (nc *NvtiCache) Init(cache string, src string, kbPath string) {
	/*
	 * Initializes the nvti cache.
	 *
	 * cache         The directory where the cache is to be stored.
	 * src           The directory that contains the nvt files.
	 * kb_path       Path to kb socket.
	 */
	nc.cachePath = cache
	nc.srcPath = src
	// cacheKb = KbNew(kbpath)  TODO
}

func (nc *NvtiCache) Free() {
	// Free the nvti cache
	nc = &NvtiCache{}
}

func (nc *NvtiCache) Get(filename string) *Nvti {
	/*
	 * Retrieve NVT Information from the nvt cache for the given filename.
	 *
	 * filename The name of the original NVT without the path
	 *          to the base location of NVTs (e.g.
	 *          "scriptname1.nasl" or even
	 *          "subdir1/subdir2/scriptname2.nasl" )
	 */
	var srcFile, cacheFile string
	//var dummy, pattern, oid string
	var srcStat, cacheStat os.FileInfo
	var errA, errB error
	var nvti *Nvti
	srcFile = filepath.Join(nc.srcPath, filename)
	cacheFile = filepath.Join(nc.cachePath, filename+".nvti")
	srcStat, errA = os.Stat(srcFile)
	cacheStat, errB = os.Stat(cacheFile)
	if os.IsExist(errA) && os.IsExist(errB) {
		if cacheStat.ModTime().After(srcStat.ModTime()) {
			nvti = NvtiFromKeyfile(cacheFile)
			return nvti
		} else {
			return nil
		}
	}
	return nil
}

// TODO : looking into this further, I'm not convinced the cache is required
// I'll come back to this later once I've reviewed the OpenVAS codebase further
