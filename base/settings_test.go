package base

import (
	gkeyfile "bitbucket.org/pkohler/gkeyfile"
	"testing"
)

func TestSettings(t *testing.T) {
	si := &SettingsIterator{}
	err := si.InitFromFile("./example/example.keyfile", "Another Group")
	if err != nil {
		t.Error(err.Error())
	}
	iface, valtype, err := si.NextValue()
	if err != nil {
		t.Errorf("Failed to get a value for %s: %s", si.Name(), err.Error())
	}
	if iface == nil || valtype != gkeyfile.G_KEY_FILE_TYPE_BOOL_LIST {
		t.Errorf("Got invalid data from SettingsIterator.Value()")
	}
	si.Cleanup()
}
