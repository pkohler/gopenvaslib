package base

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"os/user"
	"strconv"
	"syscall"
)

/* TODO: implement this for Windows */

func DropPrivileges(username string) error {
	// relaunch process as another user on *nix systems
	// unless, of course, you're already running as that user
	cur, err := user.Current()
	if cur.Uid != "0" {
		return errors.New("only root can drop privileges")
	}
	usr, err := user.Lookup(username)
	if err != nil {
		return err
	}
	if usr.Uid == cur.Uid {
		return errors.New("new UID is same as current UID")
	}
	cmd := exec.Command(os.Args[0])
	uid, err := strconv.ParseInt(usr.Uid, 10, 32)
	if err != nil {
		return err
	}
	gid, err := strconv.ParseInt(usr.Gid, 10, 32)
	if err != nil {
		return err
	}
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Credential: &syscall.Credential{
			Uid: uint32(uid),
			Gid: uint32(gid),
		},
		Setsid: true,
	}
	err = cmd.Start()
	if err != nil {
		return err
	}
	fmt.Sprintf("Respawining as user %s under process ID %d", username, cmd.Process.Pid)
	cmd.Process.Release()
	os.Exit(0)
	return err
}
