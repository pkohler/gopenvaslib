package base

import (
	"encoding/base64"
	"io/ioutil"
	"regexp"
	"testing"
)

func TestBase64(t *testing.T) {
	testfile := "example/text.file"
	b64, err := FileAsBase64(testfile)
	if err != nil {
		t.Error(err.Error())
	}
	byt, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		t.Error(err.Error())
	}
	raw, err := ioutil.ReadFile(testfile)
	if err != nil {
		t.Error(err.Error())
	}
	if string(byt) != string(raw) {
		t.Errorf("b64 conversion failed")
	}
}

func TestExportFormat(t *testing.T) {
	fname, err := ExportFilename(
		"%C-%c-%D-%F-%M-%m-%N-%T-%t-%U-%u-%%",
		"username",
		"rtype",
		"uuid",
		"2006-01-02T15:04:05",
		"2006-01-02T15:04:05",
		"rname",
		"format_name",
	)
	if err != nil {
		t.Error(err.Error())
	}
	reFormattedCorrectly := regexp.MustCompile(
		`\d{8}-\d{6}-\d{8}-format_name-\d{8}-\d{6}-rname-rtype-\d{6}-uuid-username-%`,
	)
	if !reFormattedCorrectly.MatchString(fname) {
		t.Errorf("ExportFilename didn't format correctly; received %s", fname)
	}
}
