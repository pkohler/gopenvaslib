package base

import "testing"

func TestCredentials(t *testing.T) {
	c := Credentials{}
	c.Free()
	c.AppendToUsername("usr")
	c.AppendToPassword("pwd")
	if c.Username != "usr" {
		t.Error("Failed to append to username correctly")
	}
	if c.Password != "pwd" {
		t.Error("Failed to append to password correctly")
	}
	c.Free()
	if c.Username != "" || c.Password != "" {
		t.Error("Failed to clear credentials")
	}
}
