package base

type NvtPref struct {
	/*
	 * The structure for a preference of a NVT.
	 *
	 * The elements of this structure should never be accessed directly.
	 * Only the functions corresponding to this module should be used.
	 */
	ptype string // preference type
	name  string // name of the preference
	deflt string //default value of the preference
}

func NewNVTPref(name string, ptype string, dflt string) *NvtPref {
	np := NvtPref{
		name:  name,
		ptype: ptype,
		deflt: dflt,
	}
	return &np
}

func (np *NvtPref) Free() {
	np.name = ""
	np.ptype = ""
	np.deflt = ""
}

func (np *NvtPref) Name() string {
	return np.name
}
func (np *NvtPref) Type() string {
	return np.ptype
}
func (np *NvtPref) Default() string {
	return np.deflt
}
