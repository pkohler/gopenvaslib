package base

import (
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"strings"
	"time"
)

/*
 * A large number of functions in openvas_fil.c are just basic file operations
 * which are covered by the Go standard library. This file implements only those
 * helper functions which are not available in Go by default or are easily
 * replicated in-place.
 */

func FileAsBase64(filename string) (string, error) {
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return "", err
	}
	return base64.StdEncoding.EncodeToString(b), nil
}

func ExportFilename(
	fname_format, // format string
	username, //current user name
	rtype, // type of resource
	uuid, // uuid of resource
	creation_iso_time, //creation time of resource in ISO format
	modification_iso_time, //modification time of resource (ISO)
	rname, // name or resource
	format_name string, //name of the format plugin
) (string, error) {
	// Generates a file name for exporting.
	now := time.Now()
	now_date, now_time := dateAndTimeStrings(now)
	isofmt := "2006-01-02T15:04:05" // the expected format for create/mod time
	create, err := time.Parse(isofmt, creation_iso_time)
	if err != nil {
		return "", err
	}
	create_date, create_time := dateAndTimeStrings(create)
	mod, err := time.Parse(isofmt, creation_iso_time)
	if err != nil {
		return "", err
	}
	mod_date, mod_time := dateAndTimeStrings(mod)
	fstate := 0
	outstr := ""
	for _, s := range strings.Split(fname_format, "") {
		if fstate == 0 {
			if s == "%" {
				fstate = 1
			} else if s == "\"" {
				outstr += "\\\""
			} else {
				outstr += s
			}
		} else if fstate == 1 {
			switch s {
			case "C":
				outstr += create_date
			case "c":
				outstr += create_time
			case "D":
				outstr += now_date
			case "F":
				outstr += format_name
			case "M":
				outstr += mod_date
			case "m":
				outstr += mod_time
			case "N":
				outstr += rname
			case "T":
				outstr += rtype
			case "t":
				outstr += now_time
			case "U":
				outstr += uuid
			case "u":
				outstr += username
			case "%":
				outstr += "%"
			default:
				fstate = -1
				err = fmt.Errorf(
					"%s : Unknown file name format placeholder: %%%s.",
					"ExportFilename",
					s,
				)
			}
			fstate = 0
		} else if fstate == -1 {
			return outstr, err
			break
		}
	}
	return outstr, nil
}

func dateAndTimeStrings(t time.Time) (string, string) {
	strdate := fmt.Sprintf(
		"%04d%02d%02d",
		t.Year(),
		t.Month(),
		t.Day(),
	)
	strtime := fmt.Sprintf(
		"%02d%02d%02d",
		t.Hour(),
		t.Minute(),
		t.Second(),
	)
	return strdate, strtime
}
