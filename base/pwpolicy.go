package base

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"regexp/syntax"
	"strings"
)

/**
 * @brief The name of the pattern file
 *
 * This file contains pattern with bad passphrases.  The file is line
 * based with maximum length of 255 bytes per line and expected to be
 * in UTF-8 encoding.  Each line may either be a comment line, a
 * simple string, a regular expression or a processing instruction.
 * The lines are parsed sequentially.
 *
 * *Comments* are indicated by a hash mark ('#') as the first non
 * white-space character of a line followed immediately by a space or
 * end of line.  Such a comment line is completely ignored.
 *
 * *Simple strings* start after optional leading white-space.  They
 * are compared to the password under validation.  The comparison is
 * case insensitive for all ASCII characters.
 *
 * *Regular expressions* start after optional leading white-space with
 * either a single slash ('/') or an exclamation mark ('!') directly
 * followed by a slash.  They extend to the end of the line but may be
 * terminated with another slash which may then only be followed by
 * more white-space.  The regular expression are Perl Compatible
 * Regular Expressions (PCRE) and are by default case insensitive.  If
 * the regular expression line starts with the exclamation mark, the
 * match is reversed; i.e. an error is returned if the password does
 * not match.
 *
 * *Processing instructions* are special comments to control the
 * operation of the policy checking.  The start like a comment but the
 * hash mark is immediately followed by a plus ('+') signed, a
 * keyword, an optional colon (':') and an optional value string.  The
 * following processing instructions are supported:
 *
 *   #+desc[:] STRING
 *
 *     This is used to return a meaningful error message.  STRING is
 *     used a the description for all errors up to the next /desc/ or
 *     /nodesc/ processing instruction.
 *
 *   #+nodesc
 *
 *     This is syntactic sugar for /desc/ without a value.  It
 *     switches back to a default error description (pattern file name
 *     and line number).
 *
 *   #+search[:] FILENAME
 *
 *     This searches the file with name FILENAME for a match.  The
 *     comparison is case insensitive for all ASCII characters.  This
 *     is a simple linear search and stops at the first match.
 *     Comments are not allowed in that file.  A line in that file may
 *     not be longer than 255 characters.  An example for such a file
 *     is "/usr/share/dict/words".
 *
 *   #+username
 *
 *     This is used to perform checks on the name/password
 *     combination.  Currently this checks whether the password
 *     matches or is included in the password. It may eventually be
 *     extended to further tests.
 */

const (
	DefaultPwdPolicyFile = "./pwpolicy.conf"
)

var disable_password_policy = false

func isKeyword(str, kwd string) bool {
	// Check whether a string starts with a keyword
	return strings.HasPrefix(str, kwd)
}

func searchFile(filename, pwd string) (bool, error) {
	// perform a case-insensitive search for a password in a file
	// return true if password is found, false otherwise.
	pwd = strings.ToLower(pwd)
	f, err := os.Open(filename)
	if err != nil {
		return false, err
	}
	fscanner := bufio.NewScanner(f)
	for fscanner.Scan() {
		s := strings.ToLower(fscanner.Text())
		if strings.Contains(s, pwd) {
			return true, nil
		}
	}
	return false, nil
}

func parsePatternLine(
	line string,
	descp *string,
	password string,
	username string,
) (string, error) {
	line = strings.TrimSpace(line)
	if line == "" {
		// empty line, nothing to parse
		return "", nil
	}
	if isKeyword(line, "#+desc") {
		d := strings.Replace(line, "#+desc", "", -1)
		descp = &d
	} else if isKeyword(line, "#+nodesc") {
		descp = nil
	} else if isKeyword(line, "#+search") {
		filename := strings.Replace(line, "#+search", "", -1)
		sret, err := searchFile(filename, password)
		if err != nil {
			return "", fmt.Errorf(
				"error searching '%s': %s",
				filename,
				err.Error(),
			)
		} else if descp != nil && sret {
			return fmt.Sprintf("Weak password (%s)", password, *descp), nil
		} else if sret {
			return fmt.Sprintf("Weak password (found in '%s')", filename), nil
		} else {
			return "", nil
		}
	} else if isKeyword(line, "#+username") {
		lusername := strings.ToLower(username)
		lpassword := strings.ToLower(password)
		if username == "" {
			return "", nil
		} else if lusername == lpassword {
			return "Weak password (user name matches password)", nil
		} else if strings.Contains(lpassword, lusername) {
			return "Weak password (user name is part of the password)", nil
		} else if strings.Contains(lusername, lpassword) {
			return "Weak password (password is part of the username)", nil
		} else {
			return "", nil
		}
	} else if isKeyword(line, "#") {
		// is a normal comment, not a directive
		return "", nil
	} else if isKeyword(line, "/") || isKeyword(line, "!/") {
		// is a regular expression
		reverse := isKeyword(line, "!")
		re := regexp.MustCompile(`\!?/.*/?`)
		flags := syntax.Perl | syntax.FoldCase
		pattern := re.FindString(line)
		pattre, err := regexp.Compile(fmt.Sprintf("?%v%s", flags, pattern))
		if err != nil {
			return "", err
		} else {
			match := pattre.MatchString(password)
			if (!match && reverse) || (match && !reverse) {
				// either the password doesn't match what it isn't supposed to
				// or it does match what it is supposed to
				return "", nil
			} else if descp != nil {
				return fmt.Sprintf("Weak password (%s)", *descp), nil
			} else {
				return fmt.Sprintf("Weak password (%s)", line), nil
			}
		}
	} else {
		// simple string
		if strings.ToLower(password) != strings.ToLower(line) {
			return "", nil
		} else if descp != nil {
			return fmt.Sprintf("Weak password (%s)", *descp), nil
		} else {
			return fmt.Sprintf("Weak password (%s)", line), nil
		}
	}
	// it's actually impossible for us to get here, but Go wants a return at the
	// end of the function
	return "", fmt.Errorf("Unable to parse line properly: %s", line)
}

func DisablePasswordPolicy() {
	disable_password_policy = true
}

func EnablePasswordPolicy() {
	disable_password_policy = false
}
