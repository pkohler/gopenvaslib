package base

import (
	"encoding/hex"
	"fmt"
	"math/big"
	"math/rand"
	"net"
	"sort"
	"strconv"
	"strings"
)

/*
 * Implementation of an API to handle Hosts objects
 * This file contains all methods to handle Hosts collections (Hosts)
 * and single hosts objects (Host)
 */

type host_type int32

const (
	HOST_TYPE_NAME         host_type = iota /* Hostname eg. foo */
	HOST_TYPE_IPV4                          /* eg. 192.168.1.1 */
	HOST_TYPE_CIDR_BLOCK                    /* eg. 192.168.15.0/24 */
	HOST_TYPE_RANGE_SHORT                   /* eg. 192.168.15.10-20 */
	HOST_TYPE_RANGE_LONG                    /* eg. 192.168.15.10-192.168.18.3 */
	HOST_TYPE_IPV6                          /* eg. ::1 */
	HOST_TYPE_CIDR6_BLOCK                   /* eg. ::ffee/120 */
	HOST_TYPE_RANGE6_LONG                   /* eg. ::1:200:7-::1:205:500 */
	HOST_TYPE_RANGE6_SHORT                  /* eg. ::1-fe10 */
	HOST_TYPE_MAX                           /* Boundary checking. */
)

type Host struct {
	/*
	 * The structure for a single host object.
	 * The elements of this structure should never be accessed directly.
	 * Only the functions corresponding to this module should be used.
	 */
	name string // Hostname
	/*
	 * IP address - net.IP covers both ipv4 and ipv6, so
	 * no need to have two fields like in openvas_hosts.c
	 */
	addr  *net.IP
	htype host_type // HOST_TYPE_NAME, HOST_TYPE_IPV4 or HOST_TYPE_IPV6.
}

func (h *Host) Type() host_type {
	return h.htype
}

func (h *Host) TypeStr() string {
	switch h.htype {
	case HOST_TYPE_NAME:
		return "HOST_TYPE_NAME"
	case HOST_TYPE_IPV4:
		return "HOST_TYPE_IPV4"
	case HOST_TYPE_CIDR_BLOCK:
		return "HOST_TYPE_CIDR_BLOCK"
	case HOST_TYPE_RANGE_SHORT:
		return "HOST_TYPE_RANGE_SHORT"
	case HOST_TYPE_RANGE_LONG:
		return "HOST_TYPE_RANGE_LONG"
	case HOST_TYPE_IPV6:
		return "HOST_TYPE_IPV6"
	case HOST_TYPE_CIDR6_BLOCK:
		return "HOST_TYPE_CIDR6_BLOCK"
	case HOST_TYPE_RANGE6_LONG:
		return "HOST_TYPE_RANGE6_LONG"
	case HOST_TYPE_RANGE6_SHORT:
		return "HOST_TYPE_RANGE6_SHORT"
	case HOST_TYPE_MAX:
		return "HOST_TYPE_MAX"
	default:
		return "UNKNOWN"
	}
}

func (h *Host) Name() string {
	return h.name
}

func (o *Host) String() string {
	return o.addr.String()
}

func NewHost() *Host {
	return &Host{}
}

func NewHostFromString(s string) (*Host, error) {
	/*
	 * Host structs represent an individual host, never a range,
	 * so this will return an error if you pass anything other than an IPv4/IPv6
	 * address or a resolvable hostname.
	 */
	var err error
	h := NewHost()
	h.name = s
	h.htype, err = GetHostType(s)
	if err != nil {
		return nil, err
	}
	if h.htype == HOST_TYPE_IPV4 || h.htype == HOST_TYPE_IPV6 {
		// it's an IP address type, just use net.ParseIP
		ip := net.ParseIP(s)
		h.addr = &ip
	} else if h.htype == HOST_TYPE_NAME {
		/*
		 * Resolve the domain to obtain the IP address.
		 * No need to catch the error here since it would've been caught by
		 * GetHostType(s) already.
		 */
		ipaddr, _ := net.ResolveIPAddr("ip", s)
		h.addr = &ipaddr.IP
	} else {
		return nil, fmt.Errorf("Invalid host string: %s", s)
	}
	return h, err
}

type Hosts struct {
	/*
	 * The structure for Hosts collection.
	 * The elements of this structure should never be accessed directly.
	 * Only the functions corresponding to this module should be used.
	 */
	origStr string  /* Original hosts definition string. */
	hosts   []*Host /* Hosts objects list. */
	current int64   /* index of current host object in iteration. */
	count   int64   /* Number of single host objects in hosts list. */
	removed int64   /* Number of duplicate/excluded values. */
}

func (hs *Hosts) Deduplicate() {
	// use a hash table to deduplicate the hosts list
	hash := map[string]*Host{}
	var dedupedlist []*Host
	dups := int64(0)
	for _, h := range hs.hosts {
		if hash[h.name] != nil {
			dups++
		}
		hash[h.name] = h
	}
	for _, v := range hash {
		dedupedlist = append(dedupedlist, v)
	}
	hs.hosts = dedupedlist
	hs.removed += dups
	hs.count = int64(len(hs.hosts))
}

func NewHostsWithMax(hosts_str string, max_hosts int64) (*Hosts, error) {
	/*
	 * Creates a new Hosts struct and the associated hosts
	 * objects from the provided hosts_str.
	 * hosts_str - The comma-separated hosts string. A copy will be created
	 *     of this within the returned struct.
	 * max_hosts - Max number of hosts to generate from hosts_str. 0 means unlimited.
	 */
	var err error
	oh := &Hosts{}
	oh.Free()
	oh.origStr = hosts_str
	split := strings.Split(hosts_str, ",")
	var count int64 = 0
	for _, ele := range split {
		if max_hosts != 0 && count > max_hosts {
			break
		}
		ele = strings.TrimSpace(ele)
		if ele == "" {
			continue
		}
		htype, err := GetHostType(ele)
		if err != nil {
			// invalid host string
			return nil, err
		}
		switch htype {
		case HOST_TYPE_NAME, HOST_TYPE_IPV4, HOST_TYPE_IPV6:
			// create and append a single host
			host, err := NewHostFromString(ele)
			if err != nil {
				// invalid host string
				return nil, err
			}
			oh.hosts = append(oh.hosts, host)
		case HOST_TYPE_CIDR_BLOCK, HOST_TYPE_RANGE_LONG, HOST_TYPE_RANGE_SHORT:
			// create and append a set of hosts from the string
			var first, last net.IP
			var onlyUseable bool
			if htype == HOST_TYPE_CIDR_BLOCK {
				first, last, err = CidrBlockIPs(ele)
				onlyUseable = true
			} else if htype == HOST_TYPE_RANGE_LONG {
				first, last, err = LongRangeNetworkIPs(ele)
				onlyUseable = false
			} else {
				first, last, err = ShortRangeNetworkIPs(ele)
				onlyUseable = false
			}
			if err != nil {
				// invalid string
				return nil, err
			}
			ipcount := ipToInt(first).Sub(ipToInt(first), ipToInt(last))
			if max_hosts != 0 && ipcount.Cmp(big.NewInt(max_hosts-count)) < 0 {
				fint := ipToInt(first)
				lint := fint.Add(fint, big.NewInt(max_hosts-count-1))
				last = intToIP(lint)
			}
			ips, err := ipRange(first, last, onlyUseable)
			if err != nil {
				return nil, err
			}
			hosts := []*Host{}
			for _, ip := range ips {
				newhost, _ := NewHostFromString(ip.String())
				hosts = append(hosts, newhost)
			}
			oh.hosts = append(oh.hosts, hosts...)
			count += int64(len(hosts))
		}

	}
	oh.count = int64(len(oh.hosts))
	oh.Deduplicate()
	if oh.count <= 0 {
		return nil, fmt.Errorf("Failed to parse any hosts from %s", hosts_str)
	}
	return oh, err
}

func NewHosts(hosts_str string) (*Hosts, error) {
	return NewHostsWithMax(hosts_str, 0)
}

func (hs *Hosts) Free() {
	hs.count = 0
	hs.current = 0
	hs.removed = 0
	hs.origStr = ""
	hs.hosts = []*Host{}
}

func (hs *Hosts) Current() *Host {
	// Get the current host in an Hosts struct
	return hs.hosts[hs.current]
}

func (hs *Hosts) Next() *Host {
	// Get the next host in an Hosts struct. Returns nil when there are
	// no more hosts.
	hs.current++
	if hs.current < hs.count-1 {
		return hs.Current()
	} else {
		hs.current--
		return nil
	}
}

func (hs *Hosts) Len() int {
	return len(hs.hosts)
}

func (hs *Hosts) Less(i, j int) bool {
	return ipToInt(*hs.hosts[i].addr).Cmp(ipToInt(*hs.hosts[j].addr)) < 0
}

func (hs *Hosts) Swap(i, j int) {
	hs.hosts[i], hs.hosts[j] = hs.hosts[j], hs.hosts[i]
}

func (hs *Hosts) Sort() *Hosts {
	// Sort the hosts slice numerically. Returns itself.
	sort.Sort(hs)
	hs.current = 0
	return hs
}
func (hs *Hosts) Reverse() *Hosts {
	// Reverse the sorting of the hosts slice. Returns itself.
	sort.Sort(sort.Reverse(hs))
	hs.current = 0
	return hs
}

func (hs *Hosts) Shuffle() *Hosts {
	// Randomize the order of hosts. Don't use while iterating over the list of
	// hosts as it resets the iterator.
	// Returns itself.
	for i, _ := range hs.hosts {
		j := rand.Intn(i + 1)
		if i != j {
			hs.hosts[i], hs.hosts[j] = hs.hosts[j], hs.hosts[i]
		}
	}
	hs.current = 0
	return hs
}

func (hs *Hosts) Exclude(hosts_str string) (*Hosts, error) {
	// remove a set of hosts from the list of hosts
	// returns itself and an error, if any
	excluded_hosts, err := NewHosts(hosts_str)
	if err != nil {
		return nil, err
	}
	exlcusions := excluded_hosts.hosts
	hash := map[string]*Host{}
	for _, h := range hs.hosts {
		hash[h.name] = h
	}
	for _, h := range exlcusions {
		if hash[h.name] != nil {
			hs.removed++
			hs.count--
			delete(hash, h.name)
		}
	}
	refined := make([]*Host, len(hash))
	i := 0
	for _, v := range hash {
		refined[i] = v
		i++
	}
	hs.hosts = refined
	hs.current = 0
	return hs, nil
}

func (hs *Hosts) ReverseLookupOnly() *Hosts {
	// purge any host that doesn't resolve on a reverse DNS lookup
	// returns itself
	validHosts := []*Host{}
	for _, h := range hs.hosts {
		_, err := net.LookupAddr(h.addr.String())
		if err == nil {
			validHosts = append(validHosts, h)
		} else {
			hs.count--
			hs.removed++
		}
	}
	hs.hosts = validHosts
	hs.current = 0
	return hs
}

func (hs *Hosts) ReverseLookupUnify() *Hosts {
	// deduplicate hosts that reverse-lookup to the same value, as well as any
	// with no reverse lookup value
	validHosts := []*Host{}
	hash := map[string]*Host{}
	for _, h := range hs.hosts {
		names, err := net.LookupAddr(h.addr.String())
		if err == nil {
			name := strings.Join(names, ",")
			if hash[name] != nil {
				hs.removed++
				hs.count--
			}
			hash[name] = h
		} else {
			hs.removed++
			hs.count--
		}
	}
	for _, h := range hash {
		validHosts = append(validHosts, h)
	}
	hs.hosts = validHosts
	hs.current = 0
	return hs
}

func (hs *Hosts) Count() int64 {
	return hs.count
}

func (hs *Hosts) Removed() int64 {
	return hs.removed
}

func (hs *Hosts) ContainsHost(host *Host) bool {
	// return true if hs has an equivalent host to h in its set
	for _, h := range hs.hosts {
		if h.addr.Equal(*host.addr) {
			return true
		}
	}
	return false
}

func IsIpv4Address(s string) bool {
	ip := net.ParseIP(s)
	if ip == nil {
		return false
	}
	if ip.To4() == nil {
		return false
	}
	return true
}

func IsIpv6Address(s string) bool {
	if IsIpv4Address(s) {
		return false
	}
	ip := net.ParseIP(s)
	if ip == nil {
		return false
	}
	return true
}

func IsCidrBlock(s string) bool {
	/*
	 * return true if s is a CIDR-expressed block, eg 192.168.0.1/24
	 * ParseCIDR doesn't always throw errors for ranges (eg. 192.168.0.1-10),
	 * which we don't want, so we'll check for the "/" as well as ensuring
	 * it's parse-able.
	 */
	_, _, err := net.ParseCIDR(s)
	return (err == nil && strings.Contains(s, "/"))
}

func IsShortRangeNetwork(s string) bool {
	/*
	 *Checks if a buffer points to a valid short range-expressed network.
	 * "192.168.11.1-50" is valid, "192.168.1.1-50e" and "192.168.1.1-300"
	 * are not.
	 */
	split := strings.Split(s, "-")
	if len(split) != 2 {
		return false
	}
	i, err := strconv.Atoi(split[1])
	if err != nil {
		return false
	}
	if !IsIpv4Address(split[0]) || i < 0 || i > 255 {
		return false
	}
	return true
}

func IsShortRange6Network(s string) bool {
	/*
	 * checks if s is a valid short IPv6 range-expressed network.
	 * "::200:ff:1-fee5" is valid.
	 */
	split := strings.Split(s, "-")
	if len(split) != 2 {
		return false
	}
	if !IsIpv6Address(split[0]) {
		return false
	}
	// the second part of the range must be a hexadecimal between 0 and ffff (65535)
	b, err := hex.DecodeString(split[1])
	if err != nil {
		return false
	}
	bint := big.NewInt(0).SetBytes(b).Uint64()
	if bint > 65535 {
		return false
	}
	return true
}

func ShortRangeNetworkIPs(s string) (net.IP, net.IP, error) {
	/*
	 * return the first and last IPs in a short range-expressed network string
	 * eg. "192.168.1.1-40" would give 192.168.1.1 as first and
	 * 192.168.2.40 as last.
	 */
	var err error
	split := strings.Split(s, "-")
	first := net.ParseIP(split[0])
	if first.To4() != nil {
		first = first.To4()
	}
	last := make([]byte, len(first))
	copy(last, first)
	if IsShortRange6Network(s) {
		// IPv6
		// err would already have been caught by IsShortRange6Network
		b, _ := hex.DecodeString(split[1])
		for i, _ := range b {
			last[len(last)-len(b)+i] = b[i]
		}
		return first, net.IP(last), err
	} else if IsShortRangeNetwork(s) {
		// IPv4
		// err would have been caught by IsShortRangeNetwork
		i, _ := strconv.Atoi(split[1])
		b := byte(i)
		last[len(last)-1] = b
		return first, net.IP(last), err
	} else {
		// not a valid shorthand IP range
		return nil, nil, fmt.Errorf(
			"Invalid short range network string supplied: %s",
			s,
		)
	}

}

func LongRangeNetworkIPs(s string) (net.IP, net.IP, error) {
	if IsLongRange6Network(s) || IsLongRangeNetwork(s) {
		// those checks will have verified that both IPs in the range are valid
		split := strings.Split(s, "-")
		first := net.ParseIP(split[0])
		last := net.ParseIP(split[1])
		if compareIP(first, last) < 0 {
			return first, last, nil
		} else {
			return last, first, nil
		}
	} else {
		// invalid long IP range
		return nil, nil, fmt.Errorf(
			"Invalid long range network string supplied: %s",
			s,
		)
	}
}

func IsLongRangeNetwork(s string) bool {
	/*
	 * checks if s is a valid long IPv4 range-expressed network.
	 * "192.168.12.1-192.168.13.50" is valid.
	 */
	split := strings.Split(s, "-")
	if len(split) != 2 {
		return false
	}
	if IsIpv4Address(split[0]) && IsIpv4Address(split[1]) {
		return true
	}
	return false
}

func IsLongRange6Network(s string) bool {
	/*
	 * checks if s is a valid long IPv6 range-expressed network.
	 * "::fee5-::1:530" is valid.
	 */
	split := strings.Split(s, "-")
	if len(split) != 2 {
		return false
	}
	if IsIpv6Address(split[0]) && IsIpv6Address(split[1]) {
		return true
	}
	return false
}

func CidrBlockIPs(cidrstr string) (net.IP, net.IP, error) {
	// return the first and last IPs in a CIDR block
	_, cidrNet, err := net.ParseCIDR(cidrstr)
	if err != nil {
		return nil, nil, err
	}
	first := cidrNet.IP
	if first.To4() != nil {
		first = first.To4()
	}
	last := net.IP(cidrNet.Mask)
	if last.To4() != nil {
		last = last.To4()
	}
	for i, b := range last {
		/*
		 * Perform a bitwise XOR between the first IP and the mask to flip
		 * everything except the unmasked bits; then do a bitwise NOT (eg. an
		 * XOR withand a set of 0s) to invert the whole thing, which gives us
		 * the proper last IP.
		 */
		last[i] = ^(b ^ first[i])
	}
	return net.IP(first), net.IP(last), err
}

func GetHostType(s string) (host_type, error) {
	var err error
	if strings.TrimSpace(s) == "" {
		return -1, fmt.Errorf("Empty string passed to GetHostType")
	}
	if IsIpv4Address(s) {
		return HOST_TYPE_IPV4, err
	}
	if IsIpv6Address(s) {
		return HOST_TYPE_IPV6, err
	}
	if IsCidrBlock(s) {
		return HOST_TYPE_CIDR_BLOCK, err
	}
	if IsShortRangeNetwork(s) {
		return HOST_TYPE_RANGE_SHORT, err
	}
	if IsLongRangeNetwork(s) {
		return HOST_TYPE_RANGE_LONG, err
	}
	if IsShortRange6Network(s) {
		return HOST_TYPE_RANGE6_SHORT, err
	}
	if IsLongRange6Network(s) {
		return HOST_TYPE_RANGE6_LONG, err
	}
	// see if it's a resolvable domain name
	_, err = net.ResolveIPAddr("ip", s)
	if err == nil {
		return HOST_TYPE_NAME, err
	}
	return -1, fmt.Errorf("Unable to parse %s to a valid host type", s)
}

func ipToInt(ip net.IP) *big.Int {
	if ip.To4() != nil {
		ip = ip.To4()
	}
	return big.NewInt(0).SetBytes(ip)
}

func intToIP(b *big.Int) net.IP {
	return net.IP(b.Bytes())
}

func incrementIP(ip net.IP) net.IP {
	one := big.NewInt(1)
	intIP := ipToInt(ip)
	intIP = intIP.Add(intIP, one)
	return intToIP(intIP)
}

func decrementIP(ip net.IP) net.IP {
	one := big.NewInt(1)
	intIP := ipToInt(ip)
	intIP = intIP.Sub(intIP, one)
	return intToIP(intIP)
}

func compareIP(x, y net.IP) int {
	// return -1 if x <  y; 0 if x == y; or +1 if x >  y
	xint := ipToInt(x)
	yint := ipToInt(y)
	return xint.Cmp(yint)
}

func ipRange(start, end net.IP, onlyUseable bool) ([]net.IP, error) {
	// return a slice containing all IPs between start and end.
	// works with ipv4 and ipv6 addresses
	if start == nil || end == nil {
		return nil, fmt.Errorf("start or end IP address was nil")
	}
	iparr := []net.IP{start}
	comp := compareIP(start, end)
	if comp > 0 {
		return nil, fmt.Errorf("start is less than or equal to end")
	}
	cur := start
	for compareIP(cur, end) < 0 {
		cur = incrementIP(cur)
		if !onlyUseable || ipIsUsable(cur) {
			iparr = append(iparr, cur)
		}
	}
	return iparr, nil
}

func ipIsUsable(ip net.IP) bool {
	/*
	 *
	 * Both network and broadcast addresses are skipped:
	 *  - They are _never_ used as a host address. Not being included is the
	 *    expected behaviour from users.
	 *  - When needed, short/long ranges (eg. 192.168.1.0-255) are available.
	 */
	b := []byte(ip)[len(ip)-1]
	return b != 0xff && b != 0x00
}
