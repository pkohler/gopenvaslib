package base

import (
	"net"
	"strings"
	"testing"
)

const (
	ipv4addr          = "192.168.0.1"
	ipv6addr          = "fe80::622d:5415:4476:45ae"
	ipv4rangel        = "192.168.0.1-192.168.10.20"
	ipv4ranges        = "192.168.0.1-25"
	ipv4rangesinvalid = "192.168.0.1-25f"
	ipv6rangel        = "::fee5-::1:530"
	ipv6ranges        = "::200:ff:1-fee5"
	ipv6rangesinvalid = "::200:ff:1-fffff"
	domainname        = "localhost"
	cidr4range        = "165.254.0.1/24"
	cidr6range        = "fe80::622d:5415:4476:45ae/124"
	localhost         = "127.0.0.1"
	noreverse         = "127.0.1.0"
)

func TestIPvXChecks(t *testing.T) {
	testtablev4 := map[string]bool{
		ipv4addr:          true,
		ipv6addr:          false,
		ipv4rangel:        false,
		ipv4ranges:        false,
		ipv4rangesinvalid: false,
		ipv6rangel:        false,
		ipv6ranges:        false,
		ipv6rangesinvalid: false,
	}
	testtablev6 := map[string]bool{
		ipv4addr:          false,
		ipv6addr:          true,
		ipv4rangel:        false,
		ipv4ranges:        false,
		ipv4rangesinvalid: false,
		ipv6rangel:        false,
		ipv6ranges:        false,
		ipv6rangesinvalid: false,
	}
	for k, v := range testtablev4 {
		r := IsIpv4Address(k)
		if r != v {
			t.Errorf(
				"IPv4 check gave unexpected result. Provided %v, expected %v, received %v.",
				k, v, r,
			)
		}
	}
	for k, v := range testtablev6 {
		r := IsIpv6Address(k)
		if r != v {
			t.Errorf(
				"IPv4 check gave unexpected result. Provided %v, expected %v, received %v.",
				k, v, r,
			)
		}
	}
}

func TestIPRanges(t *testing.T) {
	funcmap := map[string]func(string) bool{
		"IsShortRangeNetwork":  IsShortRangeNetwork,
		"IsLongRangeNetwork":   IsLongRangeNetwork,
		"IsShortRange6Network": IsShortRange6Network,
		"IsLongRange6Network":  IsLongRange6Network,
		"IsCidrBlock":          IsCidrBlock,
	}
	tests := map[string]map[string]bool{
		"IsLongRangeNetwork": map[string]bool{
			ipv4addr:          false,
			ipv6addr:          false,
			ipv4rangel:        true,
			ipv4ranges:        false,
			ipv4rangesinvalid: false,
			ipv6rangel:        false,
			ipv6ranges:        false,
			ipv6rangesinvalid: false,
			cidr4range:        false,
			cidr6range:        false,
			domainname:        false,
		},
		"IsShortRangeNetwork": map[string]bool{
			ipv4addr:          false,
			ipv6addr:          false,
			ipv4rangel:        false,
			ipv4ranges:        true,
			ipv4rangesinvalid: false,
			ipv6rangel:        false,
			ipv6ranges:        false,
			ipv6rangesinvalid: false,
			cidr4range:        false,
			cidr6range:        false,
			domainname:        false,
		},
		"IsShortRange6Network": map[string]bool{
			ipv4addr:          false,
			ipv6addr:          false,
			ipv4rangel:        false,
			ipv4ranges:        false,
			ipv4rangesinvalid: false,
			ipv6rangel:        false,
			ipv6ranges:        true,
			ipv6rangesinvalid: false,
			cidr4range:        false,
			cidr6range:        false,
			domainname:        false,
		},
		"IsLongRange6Network": map[string]bool{
			ipv4addr:          false,
			ipv6addr:          false,
			ipv4rangel:        false,
			ipv4ranges:        false,
			ipv4rangesinvalid: false,
			ipv6rangel:        true,
			ipv6ranges:        false,
			ipv6rangesinvalid: false,
			cidr4range:        false,
			cidr6range:        false,
			domainname:        false,
		},
		"IsCidrBlock": map[string]bool{
			ipv4addr:          false,
			ipv6addr:          false,
			ipv4rangel:        false,
			ipv4ranges:        false,
			ipv4rangesinvalid: false,
			ipv6rangel:        false,
			ipv6ranges:        false,
			ipv6rangesinvalid: false,
			cidr4range:        true,
			cidr6range:        true,
			domainname:        false,
		},
	}
	for name, table := range tests {
		for k, v := range table {
			r := funcmap[name](k)
			if r != v {
				t.Errorf(
					"%v check gave unexpected result. Provided %v, expected %v, received %v.",
					name, k, v, r,
				)
			}
		}
	}
}

func TestGetHostType(t *testing.T) {
	tests := map[string]host_type{
		"":                host_type(-1),
		ipv4addr:          HOST_TYPE_IPV4,
		ipv6addr:          HOST_TYPE_IPV6,
		ipv4rangel:        HOST_TYPE_RANGE_LONG,
		ipv4ranges:        HOST_TYPE_RANGE_SHORT,
		ipv4rangesinvalid: host_type(-1),
		ipv6rangel:        HOST_TYPE_RANGE6_LONG,
		ipv6ranges:        HOST_TYPE_RANGE6_SHORT,
		ipv6rangesinvalid: host_type(-1),
		domainname:        HOST_TYPE_NAME,
	}
	for k, v := range tests {
		h, _ := GetHostType(k)
		if h != v {
			t.Errorf(
				"HostType mismatch. Provided %v; %v expected, got %v",
				k,
				v,
				h,
			)
		}
	}
}

func TestCreateHost(t *testing.T) {
	tests := map[string]bool{
		ipv4addr:          true,
		ipv6addr:          true,
		ipv4rangel:        false,
		ipv4ranges:        false,
		ipv4rangesinvalid: false,
		ipv6rangel:        false,
		ipv6ranges:        false,
		ipv6rangesinvalid: false,
		domainname:        true,
	}
	for teststr, result := range tests {
		h, err := NewHostFromString(teststr)
		if (err == nil) != result {
			t.Errorf(
				"Error creating host from '%v'.\n\tobtained: %v\n\terror: %v",
				teststr, h, err,
			)
		}
	}
}

func TestDedup(t *testing.T) {
	oh := Hosts{}
	ex, _ := NewHostFromString(ipv4addr)
	oh.hosts = []*Host{ex, ex, ex}
	oh.Deduplicate()
	if len(oh.hosts) != 1 {
		t.Errorf("Deduplication of hosts failed, hosts list is %v", oh.hosts)
	}
	if oh.removed != 2 {
		t.Errorf("Removed count is inaccurate, should be 2, is %v", oh.removed)
	}
}

func TestFirstLastParsing(t *testing.T) {
	type test struct {
		str   string
		first net.IP
		last  net.IP
		f     func(string) (net.IP, net.IP, error)
	}
	tests := []test{
		test{
			ipv4ranges,
			net.ParseIP("192.168.0.1"),
			net.ParseIP("192.168.0.25"),
			ShortRangeNetworkIPs,
		},
		test{
			ipv6ranges,
			net.ParseIP("::200:ff:1"),
			net.ParseIP("::200:ff:fee5"),
			ShortRangeNetworkIPs,
		},
		test{
			ipv4rangel,
			net.ParseIP("192.168.0.1"),
			net.ParseIP("192.168.10.20"),
			LongRangeNetworkIPs,
		},
		test{
			ipv6rangel,
			net.ParseIP("::fee5"),
			net.ParseIP("::1:530"),
			LongRangeNetworkIPs,
		},
		test{
			cidr4range,
			net.ParseIP("165.254.0.0"),
			net.ParseIP("165.254.0.255"),
			CidrBlockIPs,
		},
		test{
			cidr6range,
			net.ParseIP("fe80::622d:5415:4476:45a0"),
			net.ParseIP("fe80::622d:5415:4476:45af"),
			CidrBlockIPs,
		},
	}
	for _, tt := range tests {
		first, last, err := tt.f(tt.str)
		if err != nil {
			t.Errorf("%v produced error %v", tt, err)
		}
		if !(first.Equal(tt.first) && last.Equal(tt.last)) {
			t.Errorf(
				"%v mismatched results. First: %v   Last: %v",
				tt,
				first,
				last,
			)
		}
	}
}

func TestNewHostsStruct(t *testing.T) {
	hoststr := strings.Join(
		[]string{
			ipv4ranges,
			ipv4rangel,
			ipv6ranges,
			ipv6rangel,
			cidr4range,
			cidr6range,
		},
		",",
	)
	_, err := NewHosts(hoststr)
	if err != nil {
		t.Errorf(
			"Failed to generate Hosts with string:\n%s\n\nerror: %s",
			hoststr,
			err.Error(),
		)

	}
	hoststr = strings.Join([]string{ipv4addr, ipv4ranges}, ",")
	max := int64(25)
	h, err := NewHostsWithMax(hoststr, max)
	if err != nil {
		t.Errorf(
			"Failed to generate Hosts with string:\n%s\n\nerror: %s",
			hoststr,
			err.Error(),
		)
	}
	if h.count > max {
		t.Errorf(
			"Incorrect # of hosts loaded - should be max of %d; was %d",
			max,
			h.count,
		)
	}
	if h.removed != 1 {
		t.Error("Failed to remove duplicate host entry")
	}
}

func TestHostsSorting(t *testing.T) {
	h, _ := NewHosts(ipv4ranges)
	h.Sort()
	if h.Current().name != ipv4addr {
		t.Errorf(
			"Incorrect first host - should be %s, was %s",
			ipv4addr,
			h.Current().name,
		)
	}
	next := incrementIP(net.ParseIP(ipv4addr)).String()
	if h.Next().name != next {
		t.Errorf(
			"Incorrect second host - should be %s, was %s",
			h.Current().name,
			next,
		)
	}
	if h.Shuffle().Current() == h.Shuffle().Current() && h.Shuffle().Current() == h.Shuffle().Current() {
		// Odds of the first entry remaining constant accross this many shuffles
		// are pretty low.
		t.Errorf("Shuffling produced identical entries repeatedly.")
	}
	a := h.Sort().Current()
	b := h.Reverse().Current()
	if a == b {
		t.Error("Reversal produced the same first entry as sorting.")
	}
}

func TestExclude(t *testing.T) {
	hs, _ := NewHosts(ipv4ranges)
	hs.Exclude(ipv4addr)
	if hs.Count() != 24 {
		t.Errorf("Too many remaining hosts: %d", hs.Count())
	}
	if hs.Removed() < 1 {
		t.Errorf("Removed count is incorrect: %d", hs.Removed())
	}

	cmp, _ := NewHostFromString(ipv4addr)
	if hs.ContainsHost(cmp) {
		t.Error("%v wasn't excluded correctly", cmp)
	}

	hs, _ = NewHosts(strings.Join([]string{localhost, noreverse}, ","))
	hs.ReverseLookupOnly()
	if hs.Len() != 1 {
		t.Error("Failed to purge unresolveable IP")
	}
}
